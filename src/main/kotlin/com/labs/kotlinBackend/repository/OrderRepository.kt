package com.labs.kotlinBackend.repository

import com.labs.kotlinBackend.model.Order
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface OrderRepository : JpaRepository<Order, Long> {

    @Query(nativeQuery = true,
        value = "select * from orders o \n" +
                "where o.is_deleted = false ")
    fun findAllAvailable(): List<Order>
}