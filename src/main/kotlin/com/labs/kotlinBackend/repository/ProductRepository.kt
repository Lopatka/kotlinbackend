package com.labs.kotlinBackend.repository

import com.labs.kotlinBackend.model.Product
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository : JpaRepository<Product, Long> {

    @Query(nativeQuery = true,
    value = "select * from products p \n" +
            "where p.is_deleted = false ")
    fun findAllAvailable(): List<Product>
}