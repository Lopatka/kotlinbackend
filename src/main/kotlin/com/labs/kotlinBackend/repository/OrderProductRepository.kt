package com.labs.kotlinBackend.repository

import com.labs.kotlinBackend.model.OrderProduct
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.math.BigInteger

@Repository
interface OrderProductRepository : JpaRepository<OrderProduct, Long> {

    @Query(
        nativeQuery = true,
        value = "select * from orders_products op \n" +
                "where op.order_id = :orderId and op.product_id = :productId"
    )
    fun findByIds(
        @Param(value = "orderId") orderId: Long,
        @Param(value = "productId") productId: Long
    ): OrderProduct

}