package com.labs.kotlinBackend.repository

import com.labs.kotlinBackend.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<User, Long> {
    fun findUserByLogin(username: String): User?

    @Query(nativeQuery = true,
        value = "select * from users u \n" +
                "where u.is_deleted = false ")
    fun findAllAvailable(): List<User>
}