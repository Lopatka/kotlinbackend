package com.labs.kotlinBackend.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
interface ReadController<T, N> {

    fun getOne(@RequestParam(value = "id") id: Long): ResponseEntity<T>

    fun list(): ResponseEntity<List<T>>
}