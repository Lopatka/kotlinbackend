package com.labs.kotlinBackend.controller.impl

import com.labs.kotlinBackend.dto.OrderCreateDTO
import com.labs.kotlinBackend.dto.OrderDTO
import com.labs.kotlinBackend.model.Order
import com.labs.kotlinBackend.service.impl.OrderService
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/orders")
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
class OrderController(private val orderService: OrderService) {

    @RequestMapping(value = ["/{id}"], method = [RequestMethod.GET], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOne(@PathVariable(value = "id") id: Long): ResponseEntity<OrderDTO> {
        return ResponseEntity.status(HttpStatus.OK).body(orderService.getOne(id))
    }

    @RequestMapping(value = ["/all"], method = [RequestMethod.GET], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun list(): ResponseEntity<List<Order>> {
        return ResponseEntity.status(HttpStatus.OK).body(orderService.listAll())
    }

    @RequestMapping(
        value = ["/all-available"],
        method = [RequestMethod.GET],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun listAvailable(): ResponseEntity<List<Order>> {
        return ResponseEntity.status(HttpStatus.OK).body(orderService.listAllAvailable())
    }

    @RequestMapping(value = ["/add"], method = [RequestMethod.POST], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun create(@RequestBody dto: OrderCreateDTO): ResponseEntity<OrderCreateDTO> {
        return ResponseEntity.status(HttpStatus.OK).body(orderService.create(dto))
    }

    @RequestMapping(
        value = ["/{id}/delete"],
        method = [RequestMethod.DELETE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun delete(id: Long): ResponseEntity<String> {
        orderService.delete(id)
        return ResponseEntity.status(HttpStatus.OK).body("Order has been successfully deleted")
    }
}