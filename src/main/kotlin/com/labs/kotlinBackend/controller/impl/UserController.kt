package com.labs.kotlinBackend.controller.impl

import com.labs.kotlinBackend.controller.ReadController
import com.labs.kotlinBackend.controller.UpdateController
import com.labs.kotlinBackend.dto.LoginDTO
import com.labs.kotlinBackend.dto.UserDTO
import com.labs.kotlinBackend.model.User
import com.labs.kotlinBackend.service.impl.UserService
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователями")
@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
class UserController(private val userService: UserService) : ReadController<User, UserDTO>,
    UpdateController<User, UserDTO> {

    @RequestMapping(value = ["/{id}"], method = [RequestMethod.GET], produces = [MediaType.APPLICATION_JSON_VALUE])
    override fun getOne(@PathVariable(value = "id") id: Long): ResponseEntity<User> {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getOne(id))
    }

    @RequestMapping(value = ["/all"], method = [RequestMethod.GET], produces = [MediaType.APPLICATION_JSON_VALUE])
    override fun list(): ResponseEntity<List<User>> {
        return ResponseEntity.status(HttpStatus.OK).body(userService.listAll())
    }

    @RequestMapping(
        value = ["/all-available"],
        method = [RequestMethod.GET],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun listAvailable(): ResponseEntity<List<User>> {
        return ResponseEntity.status(HttpStatus.OK).body(userService.listAll())
    }

    @RequestMapping(value = ["/add"], method = [RequestMethod.POST], produces = [MediaType.APPLICATION_JSON_VALUE])
    override fun create(@RequestBody dto: UserDTO): ResponseEntity<User> {
        return ResponseEntity.status(HttpStatus.OK).body(userService.create(dto))
    }

    @RequestMapping(
        value = ["/{id}/update"],
        method = [RequestMethod.PUT],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun update(
        @RequestBody dto: UserDTO,
        @PathVariable(value = "id") id: Long
    ): ResponseEntity<User> {
        return ResponseEntity.status(HttpStatus.OK).body(userService.update(dto, id))
    }

    @RequestMapping(
        value = ["/{id}/delete"],
        method = [RequestMethod.DELETE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun delete(@PathVariable(value = "id") id: Long): ResponseEntity<String> {
        userService.delete(id)
        return ResponseEntity.status(HttpStatus.OK).body("User has been successfully deleted")
    }

    @RequestMapping(value = ["/auth"], method = [RequestMethod.POST], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun auth(@RequestBody loginDTO: LoginDTO): ResponseEntity<User> {
        return if (!userService.checkPassword(loginDTO)) {
            ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null)
        } else {
            ResponseEntity.ok().body(userService.getByUserName(loginDTO.login))
        }
    }
}