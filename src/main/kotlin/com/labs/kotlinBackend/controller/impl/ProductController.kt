package com.labs.kotlinBackend.controller.impl

import com.labs.kotlinBackend.controller.ReadController
import com.labs.kotlinBackend.controller.UpdateController
import com.labs.kotlinBackend.dto.ProductDTO
import com.labs.kotlinBackend.model.Product
import com.labs.kotlinBackend.service.impl.ProductService
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/products")
@Tag(name = "Товары", description = "Контроллер для работы с товарами")
@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
class ProductController(private val productService: ProductService) :
    ReadController<Product, ProductDTO>, UpdateController<Product, ProductDTO> {

    @RequestMapping(value = ["/{id}"], method = [RequestMethod.GET], produces = [MediaType.APPLICATION_JSON_VALUE])
    override fun getOne(@PathVariable(value = "id") id: Long): ResponseEntity<Product> {
        return ResponseEntity.status(HttpStatus.OK).body(productService.getOne(id))
    }

    @RequestMapping(value = ["/all"], method = [RequestMethod.GET], produces = [MediaType.APPLICATION_JSON_VALUE])
    override fun list(): ResponseEntity<List<Product>> {
        return ResponseEntity.status(HttpStatus.OK).body(productService.listAll())
    }

    @RequestMapping(value = ["/all-available"], method = [RequestMethod.GET], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun listAvailable(): ResponseEntity<List<Product>> {
        return ResponseEntity.status(HttpStatus.OK).body(productService.listAllAvailable())
    }

    @RequestMapping(value = ["/add"], method = [RequestMethod.POST], produces = [MediaType.APPLICATION_JSON_VALUE])
    override fun create(@RequestBody dto: ProductDTO): ResponseEntity<Product> {
        return ResponseEntity.status(HttpStatus.OK).body(productService.create(dto))
    }

    @RequestMapping(value = ["/{id}/update"], method = [RequestMethod.PUT], produces = [MediaType.APPLICATION_JSON_VALUE])
    override fun update(
        @RequestBody dto: ProductDTO,
        @PathVariable(value = "id") id: Long
    ): ResponseEntity<Product> {
        return ResponseEntity.status(HttpStatus.OK).body(productService.update(dto, id))
    }

    @RequestMapping(
        value = ["/{id}/delete"],
        method = [RequestMethod.DELETE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun delete(id: Long): ResponseEntity<String> {
        productService.delete(id)
        return ResponseEntity.status(HttpStatus.OK).body("Product has been successfully deleted")
    }
}