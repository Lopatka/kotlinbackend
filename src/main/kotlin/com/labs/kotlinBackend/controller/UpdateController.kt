package com.labs.kotlinBackend.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
interface UpdateController<T, N> {
    fun create(@RequestBody dto: N): ResponseEntity<T>

    fun update(
        @RequestBody dto: N,
        @PathVariable(value = "id") id: Long
    ): ResponseEntity<T>

    fun delete(@PathVariable(value = "id") id: Long): ResponseEntity<String>
}