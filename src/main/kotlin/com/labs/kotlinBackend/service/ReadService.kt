package com.labs.kotlinBackend.service

interface ReadService<T, N> {

    fun getOne(id: Long): T

    fun listAll(): List<T>

}