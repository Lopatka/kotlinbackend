package com.labs.kotlinBackend.service.impl

import com.labs.kotlinBackend.dto.LoginDTO
import com.labs.kotlinBackend.dto.UserDTO
import com.labs.kotlinBackend.exception.DeletedOrBlockedException
import com.labs.kotlinBackend.exception.LoginExistException
import com.labs.kotlinBackend.model.Role
import com.labs.kotlinBackend.model.User
import com.labs.kotlinBackend.repository.RoleRepository
import com.labs.kotlinBackend.repository.UserRepository
import com.labs.kotlinBackend.service.ReadService
import com.labs.kotlinBackend.service.UpdateService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.webjars.NotFoundException


@Service
class UserService(
    private val userRepository: UserRepository,
    private val roleRepository: RoleRepository,
    private val bCryptPasswordEncoder: BCryptPasswordEncoder
) : ReadService<User, UserDTO>, UpdateService<User, UserDTO> {

    override fun getOne(id: Long): User = userRepository.findById(id).orElseThrow {
        NotFoundException("User with such ID: $id not found in the DB")
    }

    override fun listAll(): List<User> = userRepository.findAll()

    fun listAllAvailable(): List<User> = userRepository.findAllAvailable()

    override fun create(dto: UserDTO): User? {
        val user = userRepository.findUserByLogin(dto.login)

        if (user != null) {
            throw LoginExistException("User with such login: ${dto.login} already exist")
        }

        val role: Role = roleRepository.findById(dto.role).orElseThrow {
            NotFoundException("Role with such ID: ${dto.role} not found in the DB")
        }
        val newUser = User(
            id = 0,
            login = dto.login,
            password = bCryptPasswordEncoder.encode(dto.password),
            role = role
        )
        return userRepository.save(newUser)
    }

    override fun update(dto: UserDTO, id: Long): User? {
        val user = userRepository.findUserByLogin(dto.login)
        if (user != null) {
            throw LoginExistException("User with such login: ${dto.login} already exist")
        }
        userRepository.findById(id).orElseThrow {
            NotFoundException("User with such ID: $id not found in the DB")
        }
        val role: Role = roleRepository.findById(dto.role).orElseThrow {
            NotFoundException("Role with such ID: ${dto.role} not found in the DB")
        }
        val updatedUser = User(
            id = id,
            login = dto.login,
            password = bCryptPasswordEncoder.encode(dto.password),
            role = role
        )
        return userRepository.save(updatedUser)
    }

    override fun delete(id: Long) {
        val user = userRepository.findById(id).orElseThrow {
            NotFoundException("User with such ID: $id not found in the DB")
        }
        user.isDeleted = true
        userRepository.save(user)
    }

    fun checkPassword(loginDTO: LoginDTO): Boolean {
        return bCryptPasswordEncoder.matches(
            loginDTO.password,
            getByUserName(loginDTO.login)?.password
        )
    }

    fun getByUserName(name: String): User? {
        val user = userRepository.findUserByLogin(name)
        if (user != null) {
            if (user.isDeleted){
                throw DeletedOrBlockedException("User with such login: ${user.login} has been blocked or deleted")
            }
        }
        return user
    }
}