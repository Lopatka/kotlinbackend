package com.labs.kotlinBackend.service.impl

import com.labs.kotlinBackend.dto.ProductDTO
import com.labs.kotlinBackend.model.Product
import com.labs.kotlinBackend.repository.ProductRepository
import com.labs.kotlinBackend.service.ReadService
import com.labs.kotlinBackend.service.UpdateService
import org.springframework.stereotype.Service
import org.webjars.NotFoundException
import java.math.BigDecimal
import java.math.BigInteger

@Service
class ProductService(private val productRepository: ProductRepository) : ReadService<Product, ProductDTO>,
    UpdateService<Product, ProductDTO> {

    override fun getOne(id: Long): Product = productRepository.findById(id).orElseThrow {
        NotFoundException("Product with such ID: $id not found in the DB")
    }

    override fun listAll(): List<Product> = productRepository.findAll()

    fun listAllAvailable(): List<Product> = productRepository.findAllAvailable()

    override fun create(dto: ProductDTO): Product? {
        checkPrice(dto.price)
        val product = Product(
            id = 0,
            title = dto.title,
            price = dto.price,
        )
        return productRepository.save(product)
    }

    override fun update(dto: ProductDTO, id: Long): Product? {
        productRepository.findById(id).orElseThrow {
            NotFoundException("Product with such ID: $id not found in the DB")
        }
        checkPrice(dto.price)
        val updatedProduct = Product(
            id = id,
            title = dto.title,
            price = dto.price,
        )
        return productRepository.save(updatedProduct)
    }

    override fun delete(id: Long) {
        val product = productRepository.findById(id).orElseThrow {
            NotFoundException("Product with such ID: $id not found in the DB")
        }
        product.isDeleted = true
        productRepository.save(product)
    }


    private fun checkPrice(price: BigDecimal) {
        if (price < BigDecimal.ONE) {
            throw IllegalArgumentException(
                "Price of the product must be greater than 0"
            )
        }
    }
}