package com.labs.kotlinBackend.service.impl

import com.labs.kotlinBackend.dto.OrderCreateDTO
import com.labs.kotlinBackend.dto.OrderDTO
import com.labs.kotlinBackend.dto.OrderProductDTO
import com.labs.kotlinBackend.exception.EmptyOrderException
import com.labs.kotlinBackend.model.Order
import com.labs.kotlinBackend.model.OrderProduct
import com.labs.kotlinBackend.model.Product
import com.labs.kotlinBackend.repository.OrderProductRepository
import com.labs.kotlinBackend.repository.OrderRepository
import com.labs.kotlinBackend.repository.ProductRepository
import com.labs.kotlinBackend.repository.UserRepository
import org.springframework.stereotype.Service
import org.webjars.NotFoundException
import java.math.BigInteger

@Service
class OrderService(
    private val orderRepository: OrderRepository,
    private val userRepository: UserRepository,
    private val productRepository: ProductRepository,
    private val orderProductRepository: OrderProductRepository,
    private val productService: ProductService
) {

    fun getOne(id: Long): OrderDTO {
        val order = orderRepository.findById(id).orElseThrow {
            NotFoundException("Product with such ID: $id not found in the DB")
        }
        val orderProductDTOs: MutableList<OrderProductDTO> = mutableListOf()

        for (product in order.products) {
            orderProductDTOs.add(
                OrderProductDTO(
                    product = product,
                    amount = orderProductRepository.findByIds(id, product.id).amount
                )
            )
        }

        return OrderDTO(
            id = order.id,
            userId = order.user.id,
            address = order.address,
            phone = order.phone,
            orderProducts = orderProductDTOs
        )
    }

    fun listAll(): List<Order> = orderRepository.findAll()

    fun listAllAvailable(): List<Order> = orderRepository.findAllAvailable()

    fun create(dto: OrderCreateDTO): OrderCreateDTO {
        val user = userRepository.findById(dto.userId).orElseThrow {
            NotFoundException("User with such ID: ${dto.userId} not found in the DB")
        }
        val productIds: MutableList<Long> = mutableListOf()
        val orderProducts: MutableList<OrderProduct> = mutableListOf()

        for (orderProductDTO in dto.orderProducts) {
            productIds.add(orderProductDTO.productId)
        }

        val products = productRepository.findAllById(productIds)
        checkOrder(products, dto)

        var order = Order(
            id = 0,
            user = user,
            address = dto.address,
            phone = dto.phone,
        )

        for (orderProductDTO in dto.orderProducts) {
            if (orderProductDTO.amount < BigInteger.ONE) {
                throw IllegalArgumentException(
                    "Amount of the product with Id: ${orderProductDTO.productId} must be greater than 0"
                )
            }
            orderProducts.add(
                OrderProduct(
                    order = order,
                    product = productService.getOne(orderProductDTO.productId),
                    amount = orderProductDTO.amount
                )
            )
        }

        order = orderRepository.save(order)
        dto.id = order.id
        orderProductRepository.saveAll(orderProducts)
        return dto
    }

    fun delete(id: Long) {
        val order = orderRepository.findById(id).orElseThrow {
            NotFoundException("Order with such ID: $id not found in the DB")
        }
        order.isDeleted = true
        orderRepository.save(order)
    }

    private fun checkOrder(products: List<Product>, dto: OrderCreateDTO) {
        if (products.isEmpty()) {
            throw EmptyOrderException("Order must contain at least one product")
        }

        if (products.size != dto.orderProducts.size) {
            throw EmptyOrderException("Order must contain only existing products")
        }
    }
}