package com.labs.kotlinBackend.service

interface UpdateService<T, N> {
    fun create(dto: N): T?

    fun update(dto: N, id: Long): T?

    fun delete(id: Long)
}