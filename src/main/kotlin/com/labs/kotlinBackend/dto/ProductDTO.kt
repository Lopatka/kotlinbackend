package com.labs.kotlinBackend.dto


import java.math.BigDecimal


data class ProductDTO(
    val id: Long,

    val title: String,

    val price: BigDecimal,
)
