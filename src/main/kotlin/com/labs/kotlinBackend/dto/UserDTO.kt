package com.labs.kotlinBackend.dto


data class UserDTO (
    val id: Long,
    val login: String,
    val password: String,
    val role: Long
)


