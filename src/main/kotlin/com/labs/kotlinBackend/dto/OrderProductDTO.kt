package com.labs.kotlinBackend.dto

import com.labs.kotlinBackend.model.Product
import java.math.BigInteger

data class OrderProductDTO(
    val product: Product,
    val amount: BigInteger
)
