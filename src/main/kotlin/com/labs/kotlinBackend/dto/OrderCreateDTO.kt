package com.labs.kotlinBackend.dto


data class OrderCreateDTO(
    var id: Long,
    val userId: Long,
    val address: String,
    val phone: String,
    var orderProducts: MutableList<OrderProductCreateDTO>,
)