package com.labs.kotlinBackend.dto

import java.math.BigInteger


data class OrderProductCreateDTO(
    val productId: Long,
    val amount: BigInteger
)
