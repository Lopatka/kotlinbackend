package com.labs.kotlinBackend.dto

data class OrderDTO(
    var id: Long,
    val userId: Long,
    val address: String,
    val phone: String,
    var orderProducts: MutableList<OrderProductDTO>,
)
