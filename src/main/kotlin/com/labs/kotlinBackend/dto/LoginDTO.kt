package com.labs.kotlinBackend.dto

data class LoginDTO(
    val login: String,
    val password: String
)
