package com.labs.kotlinBackend.exception

import org.springdoc.api.ErrorMessage
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import org.webjars.NotFoundException

@ControllerAdvice
class ExceptionTranslator: ResponseEntityExceptionHandler() {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException::class)
    fun handleNotFoundException(ex: NotFoundException): ResponseEntity<ErrorMessage> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(ErrorMessage(ex.message))
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(LoginExistException::class)
    fun handleLoginExistException(ex: LoginExistException): ResponseEntity<ErrorMessage> {
        return ResponseEntity.status(HttpStatus.CONFLICT)
            .body(ErrorMessage(ex.message))
    }

    @ResponseStatus(HttpStatus.LOCKED)
    @ExceptionHandler(DeletedOrBlockedException::class)
    fun handleDeletedOrBlockedException(ex: DeletedOrBlockedException): ResponseEntity<ErrorMessage> {
        return ResponseEntity.status(HttpStatus.LOCKED)
            .body(ErrorMessage(ex.message))
    }

    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(EmptyOrderException::class)
    fun handleEmptyOrderException(ex: EmptyOrderException): ResponseEntity<ErrorMessage> {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
            .body(ErrorMessage(ex.message))
    }

    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(IllegalArgumentException::class)
    fun handleIllegalArgumentException(ex: IllegalArgumentException): ResponseEntity<ErrorMessage> {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
            .body(ErrorMessage(ex.message))
    }
}