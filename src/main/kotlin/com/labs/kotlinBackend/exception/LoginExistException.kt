package com.labs.kotlinBackend.exception

class LoginExistException(override val message : String) : Exception(message)