package com.labs.kotlinBackend.exception

class EmptyOrderException(override val message: String) : Exception(message)
