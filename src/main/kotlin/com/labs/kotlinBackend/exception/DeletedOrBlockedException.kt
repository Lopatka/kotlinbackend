package com.labs.kotlinBackend.exception

class DeletedOrBlockedException(override val message : String) : Exception(message)