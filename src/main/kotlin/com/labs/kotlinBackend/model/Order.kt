package com.labs.kotlinBackend.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.Hibernate
import javax.persistence.*

@Entity
@Table(name = "orders")
@SequenceGenerator(name = "orders_gen", sequenceName = "orders_seq", allocationSize = 1)
data class Order(
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orders_gen")
    val id: Long = 0L,

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", foreignKey = ForeignKey(name = "FK_ORDER_USER"), nullable = false)
    val user: User = User(),

    @Column(name = "address", nullable = false)
    val address: String = "",

    @Column(name = "phone", nullable = false)
    val phone: String = "",


    @Column(name = "is_deleted")
    var isDeleted: Boolean = false,

    @ManyToMany(
        cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH],
        fetch = FetchType.EAGER
    ) @JoinTable(
        name = "orders_products",
        joinColumns = [JoinColumn(name = "order_id")],
        foreignKey = ForeignKey(name = "FK_ORDERS_PRODUCTS"),
        inverseJoinColumns = [JoinColumn(name = "product_id")],
        inverseForeignKey = ForeignKey(name = "FK_PRODUCTS_ORDERS")
    )
    var products: MutableList<Product> = mutableListOf()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Order

        return id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , user = $user , address = $address , phone = $phone , isDeleted = $isDeleted )"
    }
}