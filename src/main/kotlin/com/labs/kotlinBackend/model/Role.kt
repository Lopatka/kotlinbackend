package com.labs.kotlinBackend.model

import com.labs.kotlinBackend.model.enums.UserRole
import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter
import org.hibernate.Hibernate
import javax.persistence.*

@Entity
@Table(name = "roles")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "roles_gen", sequenceName = "roles_seq", allocationSize = 1)
data class Role (
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roles_gen")
    val id: Long = 0L,

    @Enumerated(EnumType.STRING)
    @Column(name = "name", nullable = false)
    val name: UserRole? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Role

        return id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , name = $name )"
    }
}