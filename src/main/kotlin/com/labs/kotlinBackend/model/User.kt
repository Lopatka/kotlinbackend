package com.labs.kotlinBackend.model

import com.fasterxml.jackson.annotation.JsonIgnore
import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter
import org.hibernate.Hibernate
import javax.persistence.*

@Entity
@Table(name = "users")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "users_gen", sequenceName = "users_seq", allocationSize = 1)
data class User (
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_gen")
    val id: Long = 0L,

    @Column(name = "login", nullable = false)
    val login: String = "",

    @Column(name = "password", nullable = false)
    @JsonIgnore
    val password: String = "",

    @Column(name = "is_deleted")
    var isDeleted: Boolean = false,

    @Enumerated(EnumType.STRING)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id",
        foreignKey = ForeignKey(name = "FK_USER_ROLE"), nullable = false)
    val role: Role = Role(),

    @OneToMany(mappedBy = "user", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JsonIgnore
    val orders: MutableSet<Order>? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as User

        return id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , login = $login , password = $password , role = $role )"
    }
}