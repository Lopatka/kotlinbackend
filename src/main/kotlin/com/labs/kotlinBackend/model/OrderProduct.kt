package com.labs.kotlinBackend.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.Hibernate
import java.math.BigInteger
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Min

@Entity
@Table(name = "orders_products")
@IdClass(OrderProductPK::class)
data class OrderProduct(
    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", foreignKey = ForeignKey(name = "FK_ORDER_PRODUCT_ORDER"))
    @JsonIgnore
    val order: Order = Order(),

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", foreignKey = ForeignKey(name = "FK_ORDER_PRODUCT_PRODUCT"))
    val product: Product = Product(),

    @Min(1)
    @Column(
        name = "amount",
        columnDefinition = "int8 CHECK(amount > 0)",
        nullable = false
    )
    val amount: BigInteger = BigInteger.ONE,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as OrderProduct

        return order == other.order
                && product == other.product
    }

    override fun hashCode(): Int = Objects.hash(order, product);

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(order = $order , product = $product , amount = $amount )"
    }
}