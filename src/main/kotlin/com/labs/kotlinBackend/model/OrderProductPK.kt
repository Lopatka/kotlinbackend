package com.labs.kotlinBackend.model

import java.io.Serializable

data class OrderProductPK(
    val order: Long? = null,
    val product: Long? = null,
) : Serializable