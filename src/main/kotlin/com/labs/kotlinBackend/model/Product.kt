package com.labs.kotlinBackend.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.labs.kotlinBackend.dto.ProductDTO
import org.hibernate.Hibernate
import java.math.BigDecimal
import javax.persistence.*
import javax.validation.constraints.Min

@Entity
@Table(name = "products")
@SequenceGenerator(name = "products_gen", sequenceName = "products_seq", allocationSize = 1)
data class Product(
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "products_gen")
    val id: Long = 0L,

    @Column(name = "title", nullable = false)
    val title: String = "",

    @Min(1)
    @Column(name = "price", columnDefinition = "numeric(19, 2) CHECK(price > 0)", nullable = false)
    val price: BigDecimal = BigDecimal(0),


    @Column(name = "is_deleted")
    var isDeleted: Boolean = false,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Product

        return id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , title = $title , price = $price , isDeleted = $isDeleted )"
    }
}