package com.labs.kotlinBackend.model.enums

enum class UserRole(private val roleName: String) {
    ADMIN("ROLE_ADMIN"),
    USER("ROLE_USER"),
}